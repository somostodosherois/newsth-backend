# STH Backend

## Introdução

O sistema é dividido em 6 serviços:

**STH-Gateway:** Responsável por receber todas as requisições do front-end, e encaminhá-las ao serviço correspondente.

**STH-Session:** Responsável pela autenticação.

**STH-Users:** Responsável pelos usuários, permissões, e inventários.

**STH-Items:** Responsável pelos itens.

**STH-Missions:** Responsável pelas missões.

**STH-Donations:** Responsável pela integração com o gateway de pagamento.


Todas as requisições do front-end devem ser encaminhadas para o gateway. As requisições realizadas entre os serviços não passam pelo gateway.


## Rotas

## Session:

###Normal authentication  
POST /api/session/login  
input: email, password  
success output: {'status':'success','token':AccessToken} [200]  

###FB authentication  
POST /api/session/FBlogin  
header: Authorizarion Bearer FBAccessToken  
success output: {'status':'success','token':AccessToken} [200]  

###Gets a new token  
GET /api/session/refresh  
header: Authorizarion Bearer AccessToken  
success output: {'status':'success','token':AccessToken} [200]  

###Used by other services  
GET /api/session/auth  
header: Authorizarion Bearer AccessToken  
success output: {decodedAccessToken} [200]  


## Users:

###Register a new user  
POST /api/users/  
input: email, password, name(can be empty)  
success output: {user} [201]  

###Returns id, name and picture of all users (useful to make lists)  
GET /api/users/  
header: Authorizarion Bearer AccessToken  
success output: [users] [200]  

###Used by sessions service  
POST /api/users/login  
input: email, password  
success output: {user} [200]  

###Used by sessions service  
POST /api/users/FBlogin  
header: Authorizarion Bearer FBAccessToken  
success output: {user} [200,201]  

###Returns own info  
GET /api/users/me  
header: Authorizarion Bearer AccessToken  
success output: {user} [200]  

###Returns user info  
GET /api/users/<user_id>  
header: Authorizarion Bearer AccessToken  
success output: {user} [200]  

###Modify own info  
PUT /api/users/me  
header: Authorizarion Bearer AccessToken  
input: name, birthday (picture not yet...)  
success output: {user} [200]  

###Used by items service (it does the new item payment)  
POST /api/users/me/payment  
input: item_id  
success output: {status} [200]  

###Change own profile picture  
POST /api/users/me/picture  
input: file  
success output: {status} [200]  

###Get user profile picture  
GET /api/users/<user_id>/picture  
output:
  if the image is in the server:  
    image_file  
  else (facebook profile photo):  
    {picture_url}  
success output: {status} [200]  

###Give coins to user (used by donations service, and can be used by admins)  
POST /api/me/coins  
input: coins, secret (must be equal COIN_SECRET)  
success output: {status} [200]  


## Items:

###Creates a new item (permission needed)  
POST /api/items/  
header: Authorizarion Bearer AccessToken  
input: name, type, price, description, picture (url)  
success output: {item} [201]  

###Returns all items  
GET /api/items/  
header: Authorizarion Bearer AccessToken  
success output: [items] [200]  

###Creates a new item (permission needed)  
POST /api/items/  
header: Authorizarion Bearer AccessToken  
input: name, type, price, description, picture (url)  
success output: {user} [200]  

###Return items info  
GET /api/items/item_id_0,item_id_1,item_id_2,...  
header: Authorizarion Bearer AccessToken  
success output: [items] [200]  


## Missions:






###Create mission  
POST /api/missions/  
input: donee_id, title, subtitle, short_intro, description, goal, deadline
success output: {mission} [201]  

###Upload picture  
POST /api/missions/<mission_id>/picture  
input: file  
success output: {status} [200]

###Get mission picture  
GET /api/missions/picture/<picture_id>
success output: FILE [200]  

###Get all missions  
GET /api/missions/  
success output: [missions] [200]  

###Get mission  
GET /api/missions/<mission_id>  
success output: {mission} [200]  

###Get missions feed  
GET /api/missions/feed  
success output: {feed} [200]  

###Create donee  
POST /api/donees/  
input: name, gender, birthday, city, state  
success output: {donee} [201]  

###Get all donees  
GET /api/donees/  
success output: [donees] [200]


## Donations:

###Make a new donation  
POST /api/donations/  
header: Authorizarion Bearer AccessToken  
input: card_number, card_holder_name, card_expiration_date, card_cvv, value (in cents), mission_id  
success output: {transaction (response from pagar.me)} [200]  

###Get all donations  
GET /api/donations/  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

###Get all approved donations  
GET /api/donations/paid  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

###Get all donations to a specific mission  
GET /api/donations/mission/<mission_id>  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

###Get all donations made by user  
GET /api/donations/user/<user_id>  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

###Get all approved donations made by user  
GET /api/donations/user/<user_id>/paid  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]   
