#Make a new donation  
POST /api/donations/  
header: Authorizarion Bearer AccessToken  
input: card_number, card_holder_name, card_expiration_date, card_cvv, value (in cents), mission_id  
success output: {transaction (response from pagar.me)} [200]  

#Get all donations  
GET /api/donations/  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

#Get all approved donations  
GET /api/donations/paid  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

#Get all donations to a specific mission  
GET /api/donations/mission/<mission_id>  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

#Get all donations made by user  
GET /api/donations/user/<user_id>  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]  

#Get all approved donations made by user  
GET /api/donations/user/<user_id>/paid  
header: Authorizarion Bearer AccessToken  
success output: [donations] [200]   
