from flask import Blueprint, jsonify, request
import time
import urllib3

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db
from models.donation import Donation
from utils import encrypt, isValidEmail, MoneyToCoin
from config import PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, USERS_SERVICE_URL, PAYMENT_API, COIN_SECRET, ENCRYPTION_KEY, API_KEY, MISSIONS_SERVICE_URL
from guard import Auth, CheckPermission, GetUserID
import requests


bp_donations = Blueprint('bp_donations', __name__)

@bp_donations.route('/', methods = ['POST'])
@Auth
def Donate():
    user_id = GetUserID()
    card_number = request.form['card_number']
    card_holder_name = request.form['card_holder_name']
    card_expiration_date = request.form['card_expiration_date']
    card_cvv = request.form['card_cvv']
    value = request.form['value']
    mission_id = request.form['mission_id']
    mission_request = requests.get(MISSIONS_SERVICE_URL+mission_id, headers={"Authorization":request.headers.get('authorization')})
    if mission_request.status_code >= 400:
        return jsonify(mission_request.json()), mission_request.status_code
    card = {
        'card_number':card_number,
        'card_holder_name':card_holder_name,
        'card_expiration_date':card_expiration_date,
        'card_cvv':card_cvv
        }
    payload = {
        'api_key':API_KEY,
        'card_number':card_number,
        'card_holder_name':card_holder_name,
        'card_expiration_date':card_expiration_date,
        'card_cvv':card_cvv
        }
    querystring = urllib3.request.urlencode(card).replace('+','%20')

    new_card_request = requests.post(PAYMENT_API+"cards", data=payload)
    if new_card_request.status_code >= 400:
        return jsonify(new_card_request.json()), new_card_request.status_code
    card_id = new_card_request.json()['id']
    transaction = {
        'api_key':API_KEY,
        'amount':int(value),
        'card_id':card_id,
        'payment_method':'credit_card'
        }
    print(transaction)
    new_transaction_request = requests.post(PAYMENT_API+"transactions", data=transaction)
    transaction_result = new_transaction_request.json()
    donation = Donation(
        id = transaction_result['tid'],
        user_id = user_id,
        value = int(value)/100,
        mission_id = int(mission_id),
        status = transaction_result['status'],
        timestamp = time.time()
    )
    db.session.add(donation)
    if transaction_result['status'] == 'paid':
        data = {
            'secret':COIN_SECRET,
            'coins':MoneyToCoin(int(value))
        }
        add_coins_request = requests.post(USERS_SERVICE_URL+"me/coins", headers={"Authorization":request.headers.get('authorization')}, data=data)
    db.session.commit()
    return jsonify(transaction_result), new_transaction_request.status_code

@bp_donations.route('/', methods = ['GET'])
@Auth
def GetAllDonations():
    donations = Donation.query.all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200

@bp_donations.route('/paid', methods = ['GET'])
@Auth
def GetAllApprovedDonations():
    donations = Donation.query.filter_by(status = 'paid').all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200

@bp_donations.route('/mission/<int:mission_id>', methods = ['GET'])
@Auth
def GetMissionDonations(mission_id):
    donations = Donation.query.filter_by(mission_id = mission_id).all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200

@bp_donations.route('/mission/<int:mission_id>/paid', methods = ['GET'])
@Auth
def GetMissionApprovedDonations(mission_id):
    donations = Donation.query.filter_by(mission_id = mission_id, status='paid').all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200

@bp_donations.route('/user/<int:user_id>', methods = ['GET'])
@Auth
def GetUserDonations(user_id):
    donations = Donation.query.filter_by(user_id = user_id).all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200

@bp_donations.route('/user/<int:user_id>/paid', methods = ['GET'])
@Auth
def GetUserApprovedDonations(user_id):
    donations = Donation.query.filter_by(user_id = user_id, status='paid').all()
    donations = [d.toJSON() for d in donations]
    return jsonify(donations), 200
