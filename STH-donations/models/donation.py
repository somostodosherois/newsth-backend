from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Donation(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    user_id = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    value = db.Column(db.Float, unique=False, nullable=False)
    mission_id = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    status = db.Column(db.String(120), unique=False, nullable=False, default="pending")
    timestamp = db.Column(mysql.INTEGER(50), unique=False, nullable=False)


    def __repr__(self):
        return '<Donation id:%r>' % (self.id)

    def toJSON(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'value': self.value,
            'mission_id': self.mission_id,
            'status': self.status,
            'timestamp': self.timestamp
            }
