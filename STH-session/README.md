#Normal authentication  
POST /api/session/login  
input: email, password  
success output: {'status':'success','token':AccessToken} [200]  

#FB authentication  
POST /api/session/FBlogin  
header: Authorizarion Bearer FBAccessToken  
success output: {'status':'success','token':AccessToken} [200]  

#Gets a new token  
GET /api/session/refresh  
header: Authorizarion Bearer AccessToken  
success output: {'status':'success','token':AccessToken} [200]  

#Used by other services  
GET /api/session/auth  
header: Authorizarion Bearer AccessToken  
success output: {decodedAccessToken} [200]  
