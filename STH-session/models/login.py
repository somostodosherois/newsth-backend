from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Login(db.Model):
    user_id = db.Column(mysql.INTEGER(50), primary_key=True)
    timestamp = db.Column(mysql.INTEGER(50), primary_key=True, unique=False)
    ip_address = db.Column(db.Text, unique=False, nullable=False)


    def __repr__(self):
        return '<Login id:%r>' % (self.user_id)

    def toJSON(self):
        return {
            'user_id': self.user_id,
            'timestamp': self.timestamp,
            'ip_address': self.ip_address
            }
