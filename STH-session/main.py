from flask import Flask, jsonify, request, session
from flask_cors import CORS
from config import APPLICATION_PREFIX, USERS_SERVICE_URL, JWT_LIFETIME, JWT_KEY, DATABASE_URI
from prefix_middleware import PrefixMiddleware
from flask_sqlalchemy import SQLAlchemy
from utils import *
import jwt
import time

app = Flask(__name__)
CORS(app)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=APPLICATION_PREFIX)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SQLALCHEMY_POOL_SIZE'] = 100
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from models.login import Login

@app.route('/login', methods=['POST'])
def LoginMethod():
    data = {'email':request.form['email'],'password':request.form['password']}
    user = HttpRequest.post(USERS_SERVICE_URL+"login",data)
    if not user:
        return jsonify({'error':'Wrong email or password'}), 401
    token = createToken({'user_id':user['id'],'exp':int(time.time())+JWT_LIFETIME})
    new_login = Login(
        user_id = user['id'],
        timestamp = int(time.time()),
        ip_address = request.remote_addr
        )
    print(new_login)
    db.session.add(new_login)
    db.session.commit()
    return jsonify({'status':'success','token':token}), 200

#Necessita do token de acesso do FB enviado no header Authentication
@app.route('/FBlogin', methods=['POST'])
def FBLogin():
    FBtoken = request.headers.get('authorization')
    user = requests.post(USERS_SERVICE_URL+"FBlogin",data={},headers={"Authorization":FBtoken})
    if user.status_code >= 400:
        return jsonify({'error':'invalid facebook access token'}), 401
    user = user.json()
    token = createToken({'user_id':user['id'],'exp':int(time.time())+JWT_LIFETIME})
    print(user)
    return jsonify({'status':'success','token':token}), 200


@app.route('/refresh', methods=['GET'])
def refreshToken():
    if not request.headers.get('authorization'):
        return jsonify({'error':'a token is required'}), 401
    auth = request.headers.get('authorization').split(" ", 2)[1]
    try:
        token = jwt.decode(auth, JWT_KEY, algorithms=['HS256'])
    except jwt.ExpiredSignatureError:
        return jsonify({'error':'token timeout'}), 401
    except jwt.InvalidTokenError:
        return jsonify({'error':'invalid token'}), 401
    oldToken = DecodeToken(request.headers.get('authorization'))
    token = createToken({'user_id':oldToken['user_id'],'exp':int(time.time())+JWT_LIFETIME})
    return jsonify({'status':'success','token':token}), 200

@app.route('/auth', methods=['GET'])
def authToken():
    if not request.headers.get('authorization'):
        return jsonify({'error':'a token is required'}), 401
    auth = request.headers.get('authorization').split(" ", 2)[1]
    try:
        token = jwt.decode(auth, JWT_KEY, algorithms=['HS256'])
    except jwt.ExpiredSignatureError:
        return jsonify({'error':'token timeout'}), 401
    except jwt.InvalidTokenError:
        return jsonify({'error':'invalid token'}), 401
    return jsonify(token), 200

def createToken(payload):
    token = jwt.encode(payload, JWT_KEY, algorithm='HS256')
    str_token = token.decode("utf-8")
    return str_token
