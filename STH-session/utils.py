import jwt
from flask import request, jsonify
import time
from functools import wraps
import requests

from config import JWT_KEY, JWT_LIFETIME

def Auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not request.headers.get('authorization'):
            return jsonify({'error':'a token is required'}), 401
        auth = request.headers.get('authorization').split(" ", 2)[1]
        try:
            token = jwt.decode(auth, JWT_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'error':'token timeout'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'error':'invalid token'}), 401
        return f(*args, **kwargs)
    #decorated.__name__ = f.__name__
    return decorated

def DecodeToken(auth):
    token = auth.split(" ", 2)[1]
    return jwt.decode(token, JWT_KEY, algorithms=['HS256'])

class HttpRequest:
    def get(url):
        result = requests.get(url)
        if result.status_code >= 400:
            return False
        return result.json()
    def post(url,data):
        result = requests.post(url,data)
        if result.status_code >= 400:
            return False
        return result.json()
