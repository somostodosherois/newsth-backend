from flask import Blueprint, jsonify, request, send_file
import time
from werkzeug.utils import secure_filename

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db
from models.item import Item
from utils import encrypt, isValidEmail, allowed_file
from config import PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, USERS_SERVICE_URL, UPLOAD_FOLDER
from guard import Auth, CheckPermission
import requests


bp_items = Blueprint('bp_items', __name__)

@bp_items.route('/<int:item_id>/picture', methods = ['POST'])
@Auth
@CheckPermission
def ChangeItemPicture(item_id):
    item = Item.query.filter_by(id=item_id).first()
    if not item:
        return jsonify({'error':'item not found'}), 400
    if 'file' not in request.files or request.files['file'].filename == '':
        return jsonify({'error':'no image received'}), 400
    file = request.files['file']
    if not file or not allowed_file(file.filename):
        return jsonify({'error':'invalid file'}), 400
    filename = secure_filename(file.filename)
    filename = str(item_id)+'.'+filename.split('.')[1]
    file.save(os.path.join(UPLOAD_FOLDER, filename))
    item.picture = UPLOAD_FOLDER+"/"+filename
    db.session.commit()
    return jsonify(item), 200

@bp_items.route('/<int:item_id>/picture', methods = ['GET'])
@Auth
def GetItemPicture(item_id):
    item = Item.query.filter_by(id=item_id).first()
    if not item:
        return jsonify({'error':'item not found'}), 400
    if not item.picture:
        return jsonify({'error':'item picture not found'}), 400
    if len(item.picture.split('.')) > 3:
        return jsonify({'picture_url':item.picture}), 200
    return send_file(item.picture, mimetype='image/gif')

@bp_items.route('/', methods = ['POST'])
@Auth
@CheckPermission
def CreateItem():
    item = Item(
        name = request.form['name'],
        type = request.form['type'],
        price = request.form['price'],
        description = request.form['description'],
        picture = request.form['picture']
        )
    db.session.add(item)
    db.session.commit()
    return jsonify(item.toJSON()), 201

@bp_items.route('/', methods = ['GET'])
@Auth
def GetAllItems():
    items = Item.query.all()
    items = [i.toJSON() for i in items]
    return jsonify(items), 200


@bp_items.route('/<list:items_id>', methods = ['GET'])
@Auth
def GetItemsInfo(items_id):
    items = []
    for i in items_id:
        item = Item.query.filter_by(id=i).first()
        if not item:
            return jsonify({'error':"item "+str(i)+" not found"}), 400
        items.append(item.toJSON())
    return jsonify(items), 200


@bp_items.route('/<int:item_id>/buy', methods = ['POST'])
@Auth
def BuyItem(item_id):
    item = Item.query.filter_by(id=item_id).first()
    if not item:
        return jsonify({'error':"item not found"}), 400
    payment_request = requests.post(USERS_SERVICE_URL+"me/payment", data={'item_id':item_id}, headers={"Authorization":request.headers.get('authorization')})
    return jsonify(payment_request.json()), payment_request.status_code
