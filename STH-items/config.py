APPLICATION_PREFIX = "/api/items"

DATABASE_URI = "mysql+pymysql://h8f58pkpunsn4209:kdggiho6vkpk7q25@g8r9w9tmspbwmsyo.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/skbkh6o9gqzvl6z2"

PASSWORD_MIN_LENGTH = 6

PASSWORD_MAX_LENGTH = 50

UPLOAD_FOLDER = './images'

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

#SESSION_SERVICE_URL = "https://sth-session.appspot.com/api/session/"
#SESSION_SERVICE_URL = "http://localhost:5000/api/session/"
SESSION_SERVICE_URL = "http://sth-session.herokuapp.com/api/session/"

#USERS_SERVICE_URL = "http://localhost:5001/api/users/"
USERS_SERVICE_URL = "http://sth-users.herokuapp.com/api/users/"

PERMISSIONS_REQUIRED = {
    'ChangeItemPicture':['create_item'],
    'CreateItem':['create_item']
    }
