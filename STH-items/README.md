#Creates a new item (permission needed)  
POST /api/items/  
header: Authorizarion Bearer AccessToken  
input: name, type, price, description, picture (url)  
success output: {item} [201]  

#Returns all items  
GET /api/items/  
header: Authorizarion Bearer AccessToken  
success output: [items] [200]  

#Creates a new item (permission needed)  
POST /api/items/  
header: Authorizarion Bearer AccessToken  
input: name, type, price, description, picture (url)  
success output: {user} [200]  

#Return items info  
GET /api/items/item_id_0,item_id_1,item_id_2,...  
header: Authorizarion Bearer AccessToken  
success output: [items] [200]  
