from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Item(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    name = db.Column(db.String(120), unique=False, nullable=False)
    type = db.Column(db.String(120), unique=False, nullable=False)
    price = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    description = db.Column(db.Text, unique=False, nullable=False)
    picture = db.Column(db.Text, unique=False, nullable=False)


    def __repr__(self):
        return '<User id:%r name:%r price:%r>' % (self.id, self.name, self.price)

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'price': self.price,
            'description': self.description,
            'picture': self.picture
            }
