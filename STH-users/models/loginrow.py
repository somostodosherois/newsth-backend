from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class LoginRow(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    timestamp = db.Column(mysql.INTEGER(50), unique=False, nullable=False, primary_key=True)
    ip_address = db.Column(db.String(100), unique=False, nullable=False, default=False)


    def __repr__(self):
        return '<user_id:%r timestamp:%r ip:%r>' % (self.user_id, self.timestamp, self.ip_address)

    def toJSON(self):
        return {
            'user_id': self.user_id,
            'timestamp': self.timestamp,
            'ip_address': self.ip_address
            }
