from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db
from models.role import Role
from models.inventory import Inventory
from models.loginrow import LoginRow

class User(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    name = db.Column(db.String(120), unique=False, nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), unique=False, nullable=True)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    role = db.relationship("Role")
    regDate = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    coins = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    inventory = db.relationship("Inventory")
    birthday = db.Column(db.String(50), unique=False, nullable=True)
    picture = db.Column(db.Text, unique=False, nullable=True)
    loginrows = db.relationship("LoginRow")


    def __repr__(self):
        return '<User id:%r email:%r role:%r>' % (self.id, self.email, self.role)

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'role': self.role.toJSON(),
            'regDate': self.regDate,
            'coins': self.coins,
            'birthday': self.birthday,
            'picture': self.picture
            }

    def toJSONmin(self):
        return {
            'id': self.id,
            'name': self.name,
            'regDate': self.regDate,
            'picture': self.picture
            }

    def addItem(self,item_id):
        for i in self.inventory:
            if i.item_id == item_id:
                return False
        new_row = Inventory(
            user_id = self.id,
            item_id = item_id,
            equiped = False
        )
        self.inventory.append(new_row)
        return True
