from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db


class Role(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    users = db.relationship("User")
    create_donee = db.Column(db.Boolean, unique=False, nullable=False)
    create_item = db.Column(db.Boolean, unique=False, nullable=False)
    create_mission = db.Column(db.Boolean, unique=False, nullable=False)
    create_role = db.Column(db.Boolean, unique=False, nullable=False)
    give_role = db.Column(db.Boolean, unique=False, nullable=False)


    def __repr__(self):
        return 'id:%r name:%r' % (self.id,self.name)

    def toJSON(self):
        permissions = ['create_donee','create_item','create_mission','create_role','give_role']
        has_permissions = []
        for p in permissions:
            if getattr(self,p):
                has_permissions.append(p)
        return {
            'id': self.id,
            'name': self.name,
            'permissions':has_permissions
            }
