from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Inventory(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    item_id = db.Column(mysql.INTEGER(50), unique=False, nullable=False, primary_key=True)
    equipped = db.Column(db.Boolean, unique=False, nullable=False, default=False)


    def __repr__(self):
        return '<user_id:%r item_id:%r>' % (self.user_id, self.item_id)

    def toJSON(self):
        return {
            'user_id': self.user_id,
            'item_id': self.item_id,
            'equipped': self.equipped
            }
