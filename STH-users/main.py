from flask import Flask, jsonify, request, session
from flask_cors import CORS
from config import APPLICATION_PREFIX, DATABASE_URI, UPLOAD_FOLDER, MAX_CONTENT_LENGTH
from prefix_middleware import PrefixMiddleware
from flask_sqlalchemy import SQLAlchemy
from utils import *

app = Flask(__name__)
CORS(app)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=APPLICATION_PREFIX)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SQLALCHEMY_POOL_SIZE'] = 100
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH
db = SQLAlchemy(app)

from resources.users import bp_users

app.register_blueprint(bp_users)
