#Register a new user  
POST /api/users/  
input: email, password, name(can be empty)  
success output: {user} [201]  

#Returns id, name and picture of all users (useful to make lists)  
GET /api/users/  
header: Authorizarion Bearer AccessToken  
success output: [users] [200]  

#Used by sessions service  
POST /api/users/login  
input: email, password  
success output: {user} [200]  

#Used by sessions service  
POST /api/users/FBlogin  
header: Authorizarion Bearer FBAccessToken  
success output: {user} [200,201]  

#Returns own info  
GET /api/users/me  
header: Authorizarion Bearer AccessToken  
success output: {user} [200]  

#Returns user info  
GET /api/users/<user_id>  
header: Authorizarion Bearer AccessToken  
success output: {user} [200]  

#Modify own info  
PUT /api/users/me  
header: Authorizarion Bearer AccessToken  
input: name, birthday (picture not yet...)  
success output: {user} [200]  

#Used by items service (it does the new item payment)  
POST /api/users/me/payment  
input: item_id  
success output: {status} [200]  

#Change own profile picture  
POST /api/users/me/picture  
input: file  
success output: {status} [200]  

#Get user profile picture  
GET /api/users/<user_id>/picture  
output:
  if the image is in the server:  
    image_file  
  else (facebook profile photo):  
    {picture_url}  
success output: {status} [200]  

#Give coins to user (used by donations service, and can be used by admins)  
POST /api/me/coins  
input: coins, secret (must be equal COIN_SECRET)  
success output: {status} [200]  
