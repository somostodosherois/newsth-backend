from flask import Flask, jsonify, request, send_file, Response
from flask_cors import CORS
from prefix_middleware import PrefixMiddleware
from config import APPLICATION_PREFIX, SERVICES
import requests

app = Flask(__name__)
CORS(app)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=APPLICATION_PREFIX)

@app.route('/', defaults={'path': ''}, methods=['GET','POST'])
@app.route('/<path:path>', methods=['GET','POST'])
def Gateway(path):
    if path.split('/')[0] in SERVICES:
        if request.method == 'GET':
            try:
                gateway_request = requests.get(SERVICES[path.split('/')[0]]+path[len(path.split('/')[0]):], headers={"Authorization":request.headers.get('authorization')})
                return Response(gateway_request._content, mimetype=gateway_request.headers['Content-Type'], status=gateway_request.status_code)
            except:
                return jsonify({'error':'service unavailable'}),404
        if request.method == 'POST':
            if 'file' in request.files and request.files['file'].filename != '':
                file = request.files['file']
                gateway_request = requests.post(SERVICES[path.split('/')[0]]+path[len(path.split('/')[0]):], request.form, headers={"Authorization":request.headers.get('authorization')}, files=[('file',(file.filename,file))])
                return Response(gateway_request._content, mimetype=gateway_request.headers['Content-Type'], status=gateway_request.status_code)
            try:
                gateway_request = requests.post(SERVICES[path.split('/')[0]]+path[len(path.split('/')[0]):], request.form, headers={"Authorization":request.headers.get('authorization')})
                return Response(gateway_request._content, mimetype=gateway_request.headers['Content-Type'], status=gateway_request.status_code)
            except:
                return jsonify({'error':'service unavailable'}),404
    return jsonify({'error':'service not found'}),404
