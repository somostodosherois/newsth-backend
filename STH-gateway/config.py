APPLICATION_PREFIX = "/api"

SERVICES = {
#'session':"http://localhost:5000/api/session",
#'users':"http://localhost:5001/api/users",
#'items':"http://localhost:5002/api/items",
#'missions':"http://localhost:5003/api/missions",
#'donees':"http://localhost:5003/api/donees",
#'donations':"http://localhost:5004/api/donations"
'session':"https://sth-session.herokuapp.com/api/session",
'users':"https://sth-users.herokuapp.com/api/users",
'items':"https://sth-items.herokuapp.com/api/items",
'missions':"https://sth-missions.herokuapp.com/api/missions",
'donees':"https://sth-donees.herokuapp.com/api/donees",
'donations':"https://sth-donations.herokuapp.com/api/donations"
}
