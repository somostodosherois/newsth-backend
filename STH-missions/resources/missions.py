from flask import Blueprint, jsonify, request, send_file
import time
from werkzeug.utils import secure_filename
import numpy as np

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db
from models.mission import Mission
from models.donee import Donee
from models.picture import Picture
from models.video import Video
from utils import encrypt, isValidEmail, allowed_file
from config import PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, USERS_SERVICE_URL, ALLOWED_EXTENSIONS, UPLOAD_FOLDER, FEED_RECENT_MISSIONS, FEED_ALMOST_ENDING_MISSIONS, FEED_RANDOM_MISSIONS
from guard import Auth, GetUserID, CheckPermission
import requests
import pathlib


bp_missions = Blueprint('bp_missions', __name__)

@bp_missions.route('/<int:mission_id>/picture', methods = ['POST'])
@Auth
@CheckPermission
def UploadPicture(mission_id):
    mission = Mission.query.filter_by(id=mission_id).first()
    if not mission:
        return jsonify({'error':'mission not found'}), 400
    if 'file' not in request.files or request.files['file'].filename == '':
        return jsonify({'error':'no image received'}), 400
    file = request.files['file']
    print(file)
    if not file or not allowed_file(file.filename):
        return jsonify({'error':'invalid file'}), 400
    pathlib.Path(UPLOAD_FOLDER+'/'+str(mission_id)).mkdir(parents=True, exist_ok=True)
    picture = Picture(
        mission_id = mission_id,
        main_picture = False
        )
    db.session.add(picture)
    db.session.commit()
    filename = secure_filename(file.filename)
    filename = str(picture.id)+'.'+filename.split('.')[1]
    picture.path = path = UPLOAD_FOLDER+'/'+str(mission_id)+"/"+filename,
    description = ""
    try:
        description = request.form['description']
    except:
        pass
    picture.description = description
    file.save(os.path.join(UPLOAD_FOLDER+'/'+str(mission_id), filename))
    #mission.picture = UPLOAD_FOLDER+"/"+filename
    db.session.merge(picture)
    db.session.commit()
    return jsonify(picture.toJSON()), 200

@bp_missions.route('/picture/<int:picture_id>', methods = ['GET'])
@Auth
def GetPicture(picture_id):
    picture = Picture.query.filter_by(id=picture_id).first()
    if not picture:
        return jsonify({'error':'picture not found'}), 404
    return send_file(picture.path, mimetype='image/gif')

@bp_missions.route('/', methods = ['POST'])
@Auth
@CheckPermission
def CreateMission():
    donee_id = int(request.form['donee_id'])
    donee = Donee.query.filter_by(id=donee_id).first()
    if not donee:
        return jsonify({'error':"donee not found"}), 400
    mission = Mission(
        donee_id = donee_id,
        title = request.form['title'],
        subtitle = request.form['subtitle'],
        short_intro = request.form['short_intro'],
        description = request.form['description'],
        start_date = round(time.time()),
        deadline = request.form['deadline'],
        goal = request.form['goal']
    )
    db.session.add(mission)
    db.session.commit()
    return jsonify(mission.toJSON()), 201

@bp_missions.route('/', methods = ['GET'])
@Auth
def GetAllMissions():
    missions = Mission.query.all()
    missions = [m.toJSON() for m in missions]
    return jsonify(missions), 200


@bp_missions.route('/<int:mission_id>', methods = ['GET'])
@Auth
def GetMissionInfo(mission_id):
    mission = Mission.query.filter_by(id = mission_id).first()
    if not mission:
        return jsonify({'error':'mission not found'}), 404
    return jsonify(mission.toJSON()), 200

@bp_missions.route('/feed', methods = ['GET'])
@Auth
def GenerateFeed():
    missions = Mission.query.filter_by(end_date=0).all()
    missions = [m.toJSON() for m in missions]
    feed = []
    recent_missions = missions.copy()
    recent_missions.sort(key=lambda mission: mission['start_date'], reverse=False)
    ending_missions = missions.copy()
    ending_missions.sort(key=lambda mission: mission['deadline'], reverse=False)
    while len(missions)>0 and len(feed)<FEED_RANDOM_MISSIONS+FEED_RECENT_MISSIONS+FEED_ALMOST_ENDING_MISSIONS:
        if len(feed)<FEED_RECENT_MISSIONS :
            choosen = int(abs(np.random.normal(0,1)))%np.min([len(missions),FEED_RECENT_MISSIONS])
            feed.append(recent_missions[choosen])
            missions.remove(recent_missions[choosen])
            ending_missions.remove(recent_missions[choosen])
            recent_missions.remove(recent_missions[choosen])
        elif len(feed)<FEED_RECENT_MISSIONS+FEED_ALMOST_ENDING_MISSIONS:
            choosen = int(abs(np.random.normal(0,1)))%np.min([len(missions),FEED_ALMOST_ENDING_MISSIONS])
            feed.append(ending_missions[choosen])
            missions.remove(ending_missions[choosen])
            ending_missions.remove(ending_missions[choosen])
    return jsonify(feed), 200
