from flask import Blueprint, jsonify, request
import time

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db
from models.donee import Donee
from utils import encrypt, isValidEmail
from config import PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, USERS_SERVICE_URL
from guard import Auth, CheckPermission, GetUserID
import requests


bp_donees = Blueprint('bp_donees', __name__)

@bp_donees.route('/', methods = ['POST'])
@Auth
@CheckPermission
def CreateDonee():
    donee = Donee(
        name = request.form['name'],
        gender = request.form['gender'],
        birthday = request.form['birthday'],
        city = request.form['city'],
        state = request.form['state'],
    )
    db.session.add(donee)
    db.session.commit()
    return jsonify(donee.toJSON()), 201

@bp_donees.route('/', methods = ['GET'])
@Auth
def GetAllDonees():
    donees = Donee.query.all()
    donees = [d.toJSON() for d in donees]
    return jsonify(donees), 200
