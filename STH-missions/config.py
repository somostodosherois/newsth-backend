APPLICATION_PREFIX = "/api"

DATABASE_URI = "mysql+pymysql://i2welyvn5mrxdr6f:bbdohe6x11hhha9i@sp6xl8zoyvbumaa2.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/wk5bfl0sww8uyh33"

PASSWORD_MIN_LENGTH = 6

PASSWORD_MAX_LENGTH = 50

UPLOAD_FOLDER = './images'

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

MAX_CONTENT_LENGTH = 2 * 1024 * 1024
#2 MB

#SESSION_SERVICE_URL = "https://sth-session.appspot.com/api/session/"
#SESSION_SERVICE_URL = "http://localhost:5000/api/session/"
SESSION_SERVICE_URL = "http://sth-session.herokuapp.com/api/session/"

#USERS_SERVICE_URL = "http://localhost:5001/api/users/"
USERS_SERVICE_URL = "http://sth-users.herokuapp.com/api/users/"

FEED_RECENT_MISSIONS = 2
FEED_ALMOST_ENDING_MISSIONS = 2
FEED_RANDOM_MISSIONS = 0

PERMISSIONS_REQUIRED = {
    'CreateMission':['create_mission'],
    'UploadPicture':['create_mission'],
    'CreateDonee':['create_donee']
    }
