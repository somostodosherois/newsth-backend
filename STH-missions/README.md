#Change mission picture  
POST /api/missions/<mission_id>/picture  
input: file  
success output: {status} [200]  

#Get mission picture  
GET /api/missions/<mission_id>/picture  
output:
  if the image is in the server:  
    image_file  
  else (url):  
    {picture_url}  
success output: {status} [200]  

#Create mission  
POST /api/missions/  
input: donee_id, name, short_intro, description, picture(url only), video(url only), goal, start_date, deadline, end_date  
success output: {mission} [201]  

#Get all missions  
GET /api/missions/  
success output: [missions] [200]  

#Get mission  
GET /api/missions/<mission_id>  
success output: {mission} [200]  

#Create donee  
POST /api/donees/  
input: name, address  
success output: {donee} [201]  

#Get all donees  
GET /api/missions/  
success output: [donees] [200]
