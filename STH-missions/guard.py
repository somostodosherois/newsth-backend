from flask import request, jsonify
import time
from functools import wraps

from config import SESSION_SERVICE_URL, USERS_SERVICE_URL, PERMISSIONS_REQUIRED
import requests

def Auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not request.headers.get('authorization'):
            return jsonify({'error':'a token is required'}), 401
        auth = requests.get(SESSION_SERVICE_URL+"auth", headers={"Authorization":request.headers.get('authorization')})
        if auth.status_code >= 400:
            return jsonify({'error':'invalid token'}), 401
        return f(*args, **kwargs)
    #decorated.__name__ = f.__name__
    return decorated

def CheckPermission(f):
    def decorated(*args, **kwargs):
        permissions_required = PERMISSIONS_REQUIRED[f.__name__]
        user = requests.get(USERS_SERVICE_URL+"me", headers={"Authorization":request.headers.get('authorization')})
        if user.status_code >= 400:
            return jsonify({'error':'invalid token'}), 401
        user = user.json()
        for p in permissions_required:
            if not p in user['role']['permissions']:
                return jsonify({'error':'Você não tem permissão para fazer isso'}), 401
        return f(*args, **kwargs)
    decorated.__name__ = f.__name__
    return decorated

def GetUserID():
    if not request.headers.get('authorization'):
        return jsonify({'error':'a token is required'}), 401
    token = requests.get(SESSION_SERVICE_URL+"auth", headers={"Authorization":request.headers.get('authorization')})
    if token.status_code >= 400:
        return jsonify({'error':'invalid token'}), 401
    token = token.json()
    return token['user_id']
