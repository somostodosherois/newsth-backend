from flask import Flask, jsonify, request, session
from flask_cors import CORS
from config import APPLICATION_PREFIX, DATABASE_URI
from prefix_middleware import PrefixMiddleware
from flask_sqlalchemy import SQLAlchemy
from utils import *

app = Flask(__name__)
CORS(app)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=APPLICATION_PREFIX)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SQLALCHEMY_POOL_SIZE'] = 100
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from utils import ListConverter
app.url_map.converters['list'] = ListConverter

from resources.missions import bp_missions
from resources.donees import bp_donees

app.register_blueprint(bp_missions, url_prefix='/missions')
app.register_blueprint(bp_donees, url_prefix='/donees')
