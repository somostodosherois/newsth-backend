from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Picture(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True, autoincrement=True)
    mission_id = db.Column(db.Integer, db.ForeignKey('mission.id'), primary_key=True)
    path = db.Column(db.Text, nullable=True)
    description = db.Column(db.Text, unique=False, nullable=True)
    main_picture = db.Column(db.Boolean, unique=False, nullable=False)

    def toJSON(self):
        return {
            'id': self.id,
            'mission_id': self.mission_id,
            'description': self.description,
            'main_picture': self.main_picture
            }
