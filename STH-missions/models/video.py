from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Video(db.Model):
    mission_id = db.Column(db.Integer, db.ForeignKey('mission.id'), primary_key=True)
    link = db.Column(db.Text, unique=False, nullable=False)
    description = db.Column(db.Text, unique=False, nullable=True)

    def toJSON(self):
        return {
            'mission_id': self.mission_id,
            'link': self.link,
            'description': self.description
            }
