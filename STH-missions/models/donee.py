from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Donee(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    name = db.Column(db.String(250), unique=False, nullable=False)
    gender = db.Column(db.String(120), unique=False, nullable=False)
    birthday = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    city = db.Column(db.String(120), unique=False, nullable=False)
    state = db.Column(db.String(120), unique=False, nullable=False)


    def __repr__(self):
        return '<Donee id:%r>' % (self.id)

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'gender': self.gender,
            'birthday': self.birthday,
            'city': self.city,
            'state': self.state
            }
