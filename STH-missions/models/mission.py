from sqlalchemy.dialects import mysql

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

from main import db

class Mission(db.Model):
    id = db.Column(mysql.INTEGER(50), primary_key=True)
    donee_id = db.Column(db.Integer, db.ForeignKey('donee.id'))
    donee = db.relationship("Donee")
    title = db.Column(db.String(120), unique=False, nullable=False)
    subtitle = db.Column(db.String(250), unique=False, nullable=False)
    short_intro = db.Column(db.Text, unique=False, nullable=False)
    description = db.Column(db.Text, unique=False, nullable=False)
    goal = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    start_date = db.Column(mysql.INTEGER(50), unique=False, nullable=False)
    deadline = db.Column(mysql.INTEGER(50), unique=False, nullable=True)
    end_date = db.Column(mysql.INTEGER(50), unique=False, nullable=True)
    pictures = db.relationship("Picture")
    videos = db.relationship("Video")


    def __repr__(self):
        return '<Mission id:%r>' % (self.id)

    def toJSON(self):
        return {
            'id': self.id,
            'title': self.title,
            'subtitle': self.subtitle,
            'donee': self.donee.toJSON(),
            'short_intro': self.short_intro,
            'description': self.description,
            'videos': [p.toJSON() for p in self.videos],
            'pictures': [p.toJSON() for p in self.pictures],
            'goal': self.goal,
            'start_date': self.start_date,
            'deadline': self.deadline,
            'end_date': self.end_date
            }
